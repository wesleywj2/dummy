/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wx.dummy;

import com.wx.dummy.model.Sp;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author wesleywj2
 */
@Stateless
public class DummyRepo {
    private static final Logger log = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Sp> findAll(){
        log.info("Get all Sp");
        return em.createQuery("SELECT x FROM Sp x", Sp.class).getResultList();
    }
}
