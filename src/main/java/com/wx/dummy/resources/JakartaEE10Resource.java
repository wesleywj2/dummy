package com.wx.dummy.resources;

import com.wx.dummy.DummyRepo;
import com.wx.dummy.model.Sp;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author 
 */
@Path("resources")
public class JakartaEE10Resource {
    
    @Inject
    private DummyRepo dummyRepo;
    
    @GET
    @Path("/sp")
    @Produces("application/json")
    public List<Sp> allSp(){
        return dummyRepo.findAll();
    }
    
    @GET
    @Path("/dummy")
    @Produces("application/json")
    public List<Sp> dummySpList(){
        Sp eObj = null;
        List<Sp> rsList = new ArrayList<>();
        String[] names = {"Johnny","Ken","Wayne","Juras","Mecha","Windom","Limux"};
        for(String x : names){
            eObj = new Sp();
            eObj.setSpId(UUID.randomUUID());
            eObj.setSpName(x);
            eObj.setJoinDate(LocalDateTime.now());
            rsList.add(eObj);            
        }
        return rsList;
    }
}
